import { SwitchRouter } from '@react-navigation/core';
import LoginScreen from '../Login';
import Dashboard from '../Dashboard';

const Pages = SwitchRouter({
    Login: {
        screen: LoginScreen,
        path: 'login',
        isNonLogin: true,
    },
    Dashboard: {
        screen: Dashboard,
        path: 'dashboard',
    },
},
{
    initialRouteName: 'Login',
});
export default Pages;