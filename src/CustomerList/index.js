import { createSwitchNavigator } from '@react-navigation/core';
import CreateOrUpdate from './CreateOrUpdate';
import CustomerList from './List';

const navigator = createSwitchNavigator({
    List: {
        screen: CustomerList,
        path: 'list',
    },
    CreateOrUpdate: {
        screen: CreateOrUpdate,
        path: 'createorupdate',
    },
}, {
    initialRouteName: 'List',
});

export default navigator;
