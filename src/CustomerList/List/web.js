import React from 'react';
import PropTypes from 'prop-types';
import {
    Table,
} from 'reactstrap';
import FaPlus from 'react-icons/lib/fa/plus';
import {
    $headerTabStyle as headerTabStyle,
    $createButton as createButtonStyle,
    $selectNone as selectNone,
    $pointer as pointer,
} from 'Styles/globals.web.style';
import {
    map,
    get,
    isEmpty,
} from 'lodash';
import {
    getIsLoading,
    getIsError,
} from 'Utils';
import DropDownList from '../../Elements/web.contextmenucomponent';
import styles from '../web.style';

class AdminCustomerList extends React.Component {
    state={
        searchPlaceHolder: false,
    }


    hoverInfoToggle = (value) => {
        this.setState({
            [value]: !this.state[value],
        });
    }

    setEachRow = (eachObject) => {
        const {
            email,
            isActive,
            logo,
            subscriptions = [],
            name,
        } = eachObject;
        const subscriptionDetails = get(subscriptions, ['0']);
        const end = get(subscriptionDetails, ['end'], '');
        const type = get(subscriptionDetails, ['type'], '');
        const output = {
            email,
            type: !isEmpty(type) ? type : 'None',
            end: !isEmpty(end) ? end : 'None',
            isActive,
        };
        return map(output, (value, key) => {
            if (key === 'email') {
                return (
                    <td>
                        <div className='d-flex flex-row'>
                            <div className='d-flex flex-column ml-2 align-self-center'>
                                <div className='d-flex flex-row'>
                                    {name}
                                </div>
                                <div className='d-flex flex-row'>
                                    {email}
                                </div>
                            </div>
                        </div>
                    </td>
                );
            }
            if (key === 'isActive') {
                const active = 'Active';
                const inactive = 'In-Active';
                if (isActive === true) {
                    return <td style={styles.row}>{active}</td>;
                }
                return <td style={styles.row}>{inactive}</td>;
            }
            return (
                <td key={key} style={styles.row}> {value}</td>
            );
        });
    }

    renderBody = () => {
        const {
            headers,
            bordered,
            customerList,
            getIsOpen,
            toggleDropdown,
            changeState,
            modalValues,
        } = this.props;
        return (
            <div className="justify-content-start mt-3 ml-3 mr-3">
                <Table style={selectNone} className='bg-white' bordered={bordered}>
                    <thead className='border border-info borderonClick-top-0 border-left-0 border-right-0'>
                        <tr style={headerTabStyle} >
                            {map(headers, (name, index) => <th key={index}>
                                {name}
                            </th>)}
                        </tr>
                    </thead>
                    <tbody>
                        { map(customerList, (item, index) => (
                            <tr key ={index}>
                                {this.setEachRow(item)}
                                <td>
                                    <DropDownList
                                        uniqueValue= {item.id}
                                        toggleDropdown= {toggleDropdown}
                                        getIsOpen= {getIsOpen}
                                        modalValues= {modalValues}
                                        changeState= {changeState}
                                        itemIsActive= {item.isActive}
                                        activeLabel= {item.isActive ? 'Inactivate' : 'Activate'}
                                    />
                                </td>
                            </tr>
                        ))
                        }
                    </tbody>
                </Table>
            </div>);
    }

    renderList = () => {
        const {
            adminCustomerListResponse = {},
            adminCustomerListStatus = '',
        } = this.props;
        const isError = getIsError([
            [adminCustomerListResponse, adminCustomerListStatus],
        ]);
        const isLoaded = getIsLoading([adminCustomerListStatus], 'loaded');
        if (isLoaded) {
            return this.renderBody();
        }
        return null;
    }

    render() {
        const {
            createCustomer,
        } = this.props;
        return (
            <div style={selectNone}>
                <div
                    className="d-flex sticky-top align-items-center justify-content-between mt-3 ml-3 mr-3"
                    style={headerTabStyle}
                >
                    <div className='ml-2'>
                        <b>
                            Clients
                        </b>
                    </div>
                    <div className='d-flex flex-row mr-1 p-1'>
                        <div className='d-flex align-self-center mr-2'>
                            <button
                                onClick={() => createCustomer()}
                                id='createStaff'
                                style={{ ...createButtonStyle, ...pointer }}
                            >
                                <FaPlus
                                    size={22}
                                />
                            </button>
                        </div>
                    </div>
                </div>
                {this.alert()}
                {this.renderList()}
                {/* <div className='d-flex justify-content-center'>
                    { (totalNumber > pageSize) && !isEmpty(customerList)
                        ? <PaginationComponent
                            totalNumber={totalNumber}
                            pageSize={pageSize}
                            handleSelected={handleSelected}
                        />
                        : null
                    }
                </div> */}
            </div>
        );
    }
}
export default AdminCustomerList;
AdminCustomerList.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func,
    }).isRequired,
    headers: PropTypes.array,
    bordered: PropTypes.bool,
    customerList: PropTypes.array,
    toggleDropdown: PropTypes.func,
    getIsOpen: PropTypes.func,
    createCustomer: PropTypes.func,
    handleSearchSubmit: PropTypes.func,
    handleSearchChange: PropTypes.func,
    searchTerm: PropTypes.string,
    changeState: PropTypes.func,
    modalValues: PropTypes.object,
    adminCustomerListStatus: PropTypes.array,
    adminCustomerListResponse: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
    ]),
    subscribeModuleResponse: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
    ]),
    subscribeModuleStatus: PropTypes.string,
    updateCustomerResponse: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
    ]),
    updateCustomerStatus: PropTypes.string,
    removeClientResponse: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
    ]),
    removeClientStatus: PropTypes.string,
    handleSelected: PropTypes.func,
    pageSize: PropTypes.number,
    totalNumber: PropTypes.number,
};
