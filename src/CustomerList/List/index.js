import React from 'react';
import { get } from 'lodash';
import { connect } from 'Lib/connect';
import AdminCustomerListStore from '../../Stores/AdminCustomerListStore';
import PropTypes from 'prop-types';
import AdminCustomerList from './web';

@connect({
    adminCustomerListStore: AdminCustomerListStore.getStream(),
})
class CustomerList extends React.Component {
    // eslint-disable-next-line class-methods-use-this
    componentDidMount() {
        AdminCustomerListStore.load('loadAdminCustomerList');
    }

    toggleDropdown = (uniqueValue) => {
        const unique = get(this.state.adminCustomerListStore, uniqueValue, false);
        AdminCustomerListStore.set({
            [uniqueValue]: !unique,
            customerId: uniqueValue,
        });
    };

    getIsOpen = uniqueValue => get(this.state.adminCustomerListStore, uniqueValue, false)

    createCustomer = () => {
        this.props.navigation.navigate('CreateOrUpdate');
    }

    changeState = (field, ...args) => {
        const { modalValues } = this.state.adminCustomerListStore;
        const [values = {}] = args;
        const {
            activeItemId = '',
            isActive = false,
            isYes = false,
            type = '',
        } = values;
        if (activeItemId) {
            modalValues.activeItemId = activeItemId;
        }
        // eslint-disable-next-line default-case
        switch (field) {
        case 'changeActiveStatus':
            modalValues.isOpen = true;
            modalValues.header = `${isActive ? 'Inactivate' : 'Activate'} Client`;
            modalValues.body = `Are you sure want to ${isActive ? 'Inactivate' : 'Activate'} this client?`;
            modalValues.info = `Please note all staff memebers will get ${isActive ? 'inactivated' : 'activated'}`;
            modalValues.isActive = !isActive;
            modalValues.type = 'changeActiveStatus';
            break;
        case 'removeClient':
            modalValues.isOpen = true;
            modalValues.header = 'Remove Account';
            modalValues.body = 'Are you sure want to remove this account?';
            modalValues.info = 'Please note all staff memebers will get removed';
            modalValues.type = 'removeClient';
            break;
        case 'closeModal':
            if (isYes) {
                if (type === 'changeActiveStatus') {
                    AdminCustomerListStore.load('loadUpdateCustomerStatus');
                } else if (type === 'removeClient') {
                    AdminCustomerListStore.load('loadRemoveClient');
                }
            }
            modalValues.isOpen = false;
            modalValues.changeSubscription = false;
            break;
        }
        AdminCustomerListStore.set({
            modalValues,
        });
    }

    render() {
        const {
            adminCustomerListStore,
        } = this.state;
        return (
            <AdminCustomerList
                bordered={true}
                toggleDropdown={this.toggleDropdown}
                getIsOpen= {this.getIsOpen}
                createCustomer= {this.createCustomer}
                changeState= {this.changeState}
                {...adminCustomerListStore}
            />
        );
    }
}
export default CustomerList;
CustomerList.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func,
    }),
};
