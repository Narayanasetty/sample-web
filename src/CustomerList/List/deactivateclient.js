import React from 'react';
import PropTypes from 'prop-types';
import {
    Modal,
    ModalHeader,
    ModalBody,
    Button,
} from 'reactstrap';
import FaInfoCircle from 'react-icons/lib/fa/info-circle';
import {
    $selectNone as selectNone,
} from 'Styles/globals.web.style';
import styles from '../web.style';

class DeactivateClient extends React.Component {
    render() {
        const {
            isModalOpen,
            changeState,
            modalHeader = '',
            modalBody = '',
            info = '',
            type = '',
        } = this.props;
        return (
            <div style={selectNone}>
                <Modal isOpen={isModalOpen} style={selectNone}>
                    <ModalHeader
                        style ={styles.headerBackground}
                        toggle={() => changeState('closeModal')}
                        className='align-items-center p-4'
                    >
                        <div style={{
                            ...styles.buttonBackground, ...styles.headerWidth,
                        }} className = 'text-center'>
                            {modalHeader}
                        </div>
                    </ModalHeader>
                    <ModalBody
                        style ={styles.headerBackground}
                        className='p-4 align-items-center'>
                        <div style={styles.buttonBackground} className = 'text-center p-4'>
                            <div>
                                {modalBody}
                            </div>
                            <div style={{ fontSize: '12px', color: 'grey' }}>
                                <FaInfoCircle/>{info}
                            </div>
                            <div className='d-flex flex-row justify-content-around p-4'>
                                <Button
                                    style={{ ...styles.buttonColor, ...styles.modeltext }}
                                    onClick={() => changeState('closeModal', {
                                        isYes: true,
                                        type,
                                    }) }
                                > Yes
                                </Button>
                                <Button
                                    style={{ ...styles.clientButton, ...styles.modeltext }}
                                    onClick= {() => changeState('closeModal')}
                                > No
                                </Button>
                            </div>
                        </div>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}
export default DeactivateClient;
DeactivateClient.propTypes = {
    isModalOpen: PropTypes.bool,
    changeState: PropTypes.func,
    modalHeader: PropTypes.string,
    modalBody: PropTypes.string,
    info: PropTypes.string,
    type: PropTypes.string,
    uniqueValue: PropTypes.string,
};
