import React, { Component } from 'react';
import Registration from 'Registration';
import {
    $headerTabStyle as headerTabStyle,
    $routeStyle as routeStyle,
    $selectNone as selectNone,
} from 'Styles/globals.web.style';
import PropTypes from 'prop-types';

class ClientCreateOrUpdate extends Component {
    onClickHeading = () => {
        this.props.navigation.navigate('List');
    }

    render() {
        return (
            <div style={selectNone}>
                <div
                    className="d-flex sticky-top align-items-center justify-content-start mt-3 ml-3 mr-3"
                    style={headerTabStyle}
                >
                    <div className='ml-2'>
                        <b
                            onClick={() => this.onClickHeading()}
                            style={routeStyle}
                        >
                           Clients
                        </b>
                    </div>
                    &nbsp;&gt;&nbsp;{'Create'}
                </div>
                <Registration
                    isAdmin={true}
                    navigation= {this.props.navigation }
                />
            </div>
        );
    }
}

export default ClientCreateOrUpdate;
ClientCreateOrUpdate.propTypes = {
    navigation: PropTypes.object,
};
