const styles = {
    profile: {
        height: '50px',
        width: '50px',
    },
    iconStyle: {
        height: '40px',
        width: '40px',
    },
    row: {
        verticalAlign: 'inherit',
        cursor: 'pointer',
    },
    headerBackground: {
        backgroundColor: 'grey',
    },
    bodyBackground: {
        backgroundColor: '#CDCDCD',
    },
    buttonBackground: {
        backgroundColor: 'white',
    },
    textColor: {
        color: 'black',
    },
    headerWidth: {
        width: '325%',
    },
    buttonColor: {
        backgroundColor: 'red',
    },
    modeltext: {
        color: 'white',
    },
    clientButton: {
        backgroundColor: 'greeb',
    },
};
export default styles;
