const dashboardStyle = {
    scroll: {
        height: window.innerHeight - 85,
        overflowY: 'auto',
    },
    bgColor: {
        backgroundColor: 'grey',
    },
};

export default dashboardStyle;
