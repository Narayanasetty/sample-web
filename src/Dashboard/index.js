import React from 'react';
import { createNavigator, SwitchRouter } from '@react-navigation/core';
import CustomerList from 'Admin/CustomerList';
import NavigationView from './navigationview';

const router = SwitchRouter({
    CustomerList: {
        screen: CustomerList,
        path: 'customerlist',
    },
}, {});

const NavigatorComponent = createNavigator(
    props => (
        <NavigationView
            {...props}
        />
    ),
    router,
    {},
);

export default NavigatorComponent;
