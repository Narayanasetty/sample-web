import React from 'react';
import { SceneView } from '@react-navigation/core';
import PropTypes from 'prop-types';
import { connect } from 'Lib/connect';
import { get } from 'lodash';
import AdminUserStore from '../Stores/UserStore';
import dashboardStyle from './style';

@connect({
    userStore: AdminUserStore.getStream(),
})
class NavigationView extends React.Component {
    render() {
        const { state } = this.props.navigation;
        const activeKey = state.routes[state.index].key;
        const descriptor = this.props.descriptors[activeKey];
        const ScreenComponent = descriptor.getComponent();
        const userDetails = get(this.state.userStore, ['userDetails'], {});
        const screenProps = {};
        screenProps.userDetails = userDetails;
        return (
                <div className="d-flex flex-nowrap" style={dashboardStyle.bgColor}>
                    <div className="flex-grow-1 content-container">
                        <div className="d-flex">
                            <div className="flex-grow-1" style={ dashboardStyle.scroll }>
                                <SceneView
                                    component={ScreenComponent}
                                    navigation={descriptor.navigation}
                                    screenProps = {screenProps}
                                />
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}

NavigationView.propTypes = {
    navigation: PropTypes.shape({
        state: PropTypes.shape(
            {
                routes: PropTypes.arrayOf[{
                    key: PropTypes.string,
                }],
                index: PropTypes.number,
            },
        ),
    }).isRequired,
    descriptors: PropTypes.shape().isRequired,
    sideMenu: PropTypes.object,
    userDetails: PropTypes.shape({
        permissions: PropTypes.array,
    }),
};

export default NavigationView;
