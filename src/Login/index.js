import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'Lib/connect';
import AdminLoginStore from '../Stores/LoginStore';
import { eventStream } from '../Streams';
import { validateInput } from '../Utils';
import {
    isEmpty,
    find,
} from 'lodash';
import LoginScreenRender from './web';

@connect({
    loginStore: AdminLoginStore.getStream(),
})
class Login extends React.Component {
    state = {
        showPassword: false,
        emailVerificationMessage: '',
    }

    constructor(props) {
        super(props);
        AdminLoginStore.resetData();
        this.navigateStream = eventStream.subscribe((name) => {
            // eslint-disable-next-line default-case
            switch (name) {
            case 'userLoaded':
                props.navigation.navigate('Dashboard');
                break;
            }
        });
    }

    onClickShowPassword = (type) => {
        const value = this.state[type];
        this.setState({
            [type]: !value,
        });
    }

    onChange = (value, type) => {
        const fieldData = this.state.loginStore[type];
        fieldData.value = value;
        AdminLoginStore.set({
            [type]: fieldData,
        });
        this.doValidate(type, true);
    }

    onFocus = (type) => {
        const fieldData = this.state.loginStore[type];
        // fieldData.touched = true;
        AdminLoginStore.set({
            [type]: fieldData,
        });
    }

    doValidate = (type, onChange) => {
        const fieldData = this.state.loginStore[type];
        const isValid = validateInput(fieldData);
        if (isValid) {
            fieldData.isValid = true;
            fieldData.isInValid = false;
        } else if (!onChange || fieldData.touched) {
            fieldData.isValid = false;
            fieldData.isInValid = true;
            fieldData.touched = true;
        }
        AdminLoginStore.set({
            [type]: fieldData,
        });
    }

    doLogin = (e) => {
        e.preventDefault();
        AdminLoginStore.load('login');
    }

    setEmailVerificationMessage = (emailVerificationMessage) => {
        this.setState({
            emailVerificationMessage,
        });
    }

    onCloseResponseInfo = () => {
        AdminLoginStore.set({
            loginResponse: {},
            loginStatus: '',
        });
    }

    loginButtonIsDisable = () => {
        const fieldData = this.state.loginStore;
        const inValid = find(fieldData, (value) => {
            if (value.isRequired) {
                return !value.isValid;
            }
            return false;
        });
        if (isEmpty(inValid)) {
            return false;
        }
        return true;
    }

    render() {
        const { loginStatus } = this.state.loginStore;
        return (
            <LoginScreenRender
                doLogin={this.doLogin}
                {...this.state.loginStore}
                onChange={this.onChange}
                doValidate={this.doValidate}
                loginButtonIsDisable = {this.loginButtonIsDisable}
                navigation={this.props.navigation}
                setEmailVerificationMessage= {this.setEmailVerificationMessage}
                emailVerificationMessage = {this.state.emailVerificationMessage}
                isFetching={ loginStatus === 'loading'}
                onClickShowPassword = {this.onClickShowPassword}
                showPassword = {this.state.showPassword}
                onFocus= {this.onFocus}
                onCloseResponseInfo= {this.onCloseResponseInfo}
            />);
    }
}

Login.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
    }).isRequired,
};

export default Login;
