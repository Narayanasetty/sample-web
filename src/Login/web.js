import React, { Component } from 'react';
import {
    Button,
    Form,
    FormGroup,
    Input,
} from 'reactstrap';
import { get } from 'lodash';
import FaEye from 'react-icons/lib/fa/eye';
import FaEyeSlash from 'react-icons/lib/fa/eye-slash';
import PropTypes from 'prop-types';
import {
    $root as root,
    $formContainer as formContainer,
    $showPasswordIconStyle as showPasswordIconStyle,
    $showPasswordDivHeight as showPasswordDivHeight,
    $selectNone as selectNone,
} from '../Styles/globals.web.style';

class LoginScreenRender extends Component {
    componentDidMount = () => {
        const emailVerificationMessage = get(this.props.navigation, 'state.params.message', '');
        this.props.setEmailVerificationMessage(emailVerificationMessage);
    }

    render() {
        const {
            doLogin,
            email,
            doValidate,
            // onChange,
            password,
            isFetching,
            loginButtonIsDisable,
            onClickShowPassword,
            showPassword,
            onFocus,
        } = this.props;
        return (
            <div style={{ ...root, ...selectNone }} className="d-flex flex-column">
                <div style={formContainer} className="d-flex flex-column align-items-center justify-content-center">
                    <h4>Admin Login</h4>
                        <Form onSubmit={doLogin}>
                            <FormGroup>
                                <Input
                                    value={email.value}
                                    // onChange={ event => onChange(event.currentTarget.value.toLowerCase(), 'email')}
                                    onBlur={ () => doValidate('email') }
                                    onFocus={() => onFocus('email')}
                                    type="text"
                                    valid={email.isValid}
                                    invalid={email.isInValid}
                                    autoCapitalize='none'
                                    required
                                    name="email"
                                    placeholder="user@company.com"
                                />
                            </FormGroup>
                            <FormGroup>
                                <Input
                                    value={password.value}
                                    // onChange={ event => onChange(event.currentTarget.value, 'password') }
                                    onBlur={ () => doValidate('password') }
                                    onFocus={() => onFocus('password')}
                                    type= {showPassword ? 'text' : 'password'}
                                    valid={password.isValid}
                                    invalid={password.isInValid}
                                    required
                                    name='password'
                                    placeholder="*****"
                                />
                                {
                                    password.touched
                                        ? <div className='d-flex justify-content-end mr-4' style={showPasswordDivHeight}>
                                            {
                                                showPassword
                                                    ? <FaEye
                                                        onClick={() => onClickShowPassword('showPassword')}
                                                        style={showPasswordIconStyle}
                                                    />
                                                    : <FaEyeSlash
                                                        onClick={() => onClickShowPassword('showPassword')}
                                                        style={showPasswordIconStyle}
                                                    />
                                            }
                                        </div>
                                        : null
                                }
                            </FormGroup>
                           
                            <div className="d-flex justify-content-between align-items-center">
                                <Button color="success" size="sm" type="submit" disabled= {loginButtonIsDisable() || isFetching }>
                                    {isFetching ? 'Loading' : 'Login'}
                                </Button>
                            </div>
                        </Form>
                </div>
            </div>
        );
    }
}

LoginScreenRender.propTypes = {
    doLogin: PropTypes.func.isRequired,
    onClickShowPassword: PropTypes.func,
    showPassword: PropTypes.bool,
    email: PropTypes.shape({
        value: PropTypes.string.isRequired,
        isValid: PropTypes.bool,
        isInValid: PropTypes.bool,
    }),
    password: PropTypes.shape({
        value: PropTypes.string.isRequired,
        isValid: PropTypes.bool,
        isInValid: PropTypes.bool,
        touched: PropTypes.bool,
    }),
    onChange: PropTypes.func.isRequired,
    isFetching: PropTypes.bool,
    doValidate: PropTypes.func.isRequired,
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
    }),
    loginButtonIsDisable: PropTypes.func,
    onFocus: PropTypes.func,
};
export default LoginScreenRender;
