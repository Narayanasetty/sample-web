import {
    isFunction,
    isArray,
    isEmpty,
    find,
    get,
} from 'lodash';

export const getConfig = () => window.CONFIG;

export function validateInput(input) {
    if (!input.isRequired) {
        const isValid = input.value === null || input.value === '' || input.value === undefined;
        if (isValid) {
            return true;
        }
    }
    if (input.isRequired && !input.regex && !isFunction(input.regex)) {
        return !(input.value === null || input.value === '' || input.value === undefined || (isArray(input.value) && isEmpty(input.value)));
    }
    if (input.value === false) {
        return false;
    }

    if (isFunction(input.regex)) {
        return input.regex(input.value);
    }

    if (input.regex) {
        return input.regex.test(input.value);
    }
    return true;
}

export const getIsLoading = (responseArray, status) => {
    const isLoading = find(responseArray, value => value === status) || '';
    if (!isEmpty(isLoading)) {
        return true;
    }
    return false;
};

export const getIsError = (responseArray) => {
    const responseObject = find(responseArray, value => (
        get(value, ['0', 'status'], '') === 'error'
        || get(value, ['1'], '') === 'error')) || [];
    if (!isEmpty(responseObject)) {
        return {
            error: true,
            message: get(responseObject[0], ['message'], ''),
        };
    }
    return {
        error: false,
    };
};