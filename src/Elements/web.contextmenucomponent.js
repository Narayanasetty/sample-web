import React from 'react';
import PropTypes from 'prop-types';
import FaEllipsisV from 'react-icons/lib/fa/ellipsis-v';
import {
    $mayaBlue as mayaBlue,
    $bg_red as red,
} from 'Styles/globals.style';
import {
    Dropdown,
    DropdownItem,
    DropdownToggle,
    DropdownMenu,
} from 'reactstrap';
import DeactivateClient from '../CustomerList/List/deactivateclient';
import styles from './web.style';

class DropDownList extends React.Component {
    render() {
        const {
            uniqueValue,
            toggleDropdown,
            getIsOpen,
            changeState,
            activeLabel,
            itemIsActive,
            modalValues,
        } = this.props;
        const {
            isOpen = false,
            header = '',
            body = '',
            info = '',
            type = '',
        } = modalValues;
        return (
            <div>
                <Dropdown
                    isOpen= {getIsOpen(uniqueValue)}
                    toggle={() => toggleDropdown(uniqueValue)}
                >
                    <DropdownToggle nav style={styles.iconPadding}>
                        <FaEllipsisV size={20} style={ styles.iconStyle } />
                    </DropdownToggle>
                    <DropdownMenu right style={styles.toggleWidth}>
                        <DropdownItem
                            onClick={() => changeState('changeActiveStatus', {
                                isActive: itemIsActive,
                                activeItemId: uniqueValue,
                            })}
                        >
                            {activeLabel}
                        </DropdownItem>
                        <DropdownItem
                            style={{ color: red }}
                            onClick ={() => changeState('removeClient', {
                                activeItemId: uniqueValue,
                            })}
                        >
                            Remove Account
                        </DropdownItem>
                    </DropdownMenu>
                </Dropdown>
                <DeactivateClient
                    isModalOpen={isOpen}
                    changeState={changeState}
                    modalHeader={header}
                    modalBody= {body}
                    info={info}
                    type={type}
                />
            </div>
        );
    }
}
export default DropDownList;
DropDownList.propTypes = {
    uniqueValue: PropTypes.string,
    toggleDropdown: PropTypes.func,
    getIsOpen: PropTypes.func,
    navigation: PropTypes.shape({
        navigate: PropTypes.func,
    }).isRequired,
    changeState: PropTypes.func,
    deactiveClient: PropTypes.bool,
    changeSubscription: PropTypes.bool,
    activeLabel: PropTypes.string,
    itemIsActive: PropTypes.bool,
    modalValues: PropTypes.object,
    type: PropTypes.string,
};
