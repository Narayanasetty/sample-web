const styles = {
    iconPadding: {
        marginLeft: '-15px',
        width: '38px',
    },
    iconStyle: {
        width: 45,
        height: 45,
        verticalAlign: -3,
        alignItems: 'center',
        color: 'gray',
    },
    toggleWidth: {
        minWidth: '50px',
        zIndex: '1030',
    },
};
export default styles;