import {
    $lite_grey as litegrey,
    $gray_95 as lightgray,
    $Cmyk as blue,
    $grey as grey,
    $bg_red as red,
    $cl_lavender as clLavendar,
} from './globals.style';

export const $createButton = {
    height: '30px',
};

export const $headerTabStyle = {
    backgroundColor: litegrey,
    height: '40px',
};
export const $selectNone = {
    userSelect: 'none',
};

export const $routeStyle = {
    color: blue,
    cursor: 'pointer',
};

export const $pointer = {
    cursor: 'pointer',
};

export const $root = {
    backgroundColor: clLavendar,
};

export const $formContainer = {
    minHeight: 'calc(100vh - 35px)',
};

export const $showPasswordIconStyle = {
    height: '15px',
    width: '15px',
    position: 'relative',
    bottom: '27px',
    right: '6px',
};
export const $showPasswordDivHeight = {
    height: '0px',
};
