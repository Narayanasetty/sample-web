import Store from 'Lib/store';
import {
    getConfig,
} from 'Utils/index';
import {
    get,
    map,
    clone,
    filter,
} from 'lodash';


const CONFIG = getConfig();

const fields = {
    type: {
        value: '',
        isValid: false,
        isInValid: false,
        isTouched: false,
        isRequired: true,
    },
};

const AdminCustomerListStore = new Store(
    {
        modalValues: {
            modalIsOpen: false,
            changeSubscription: false,
        },
        module: 'jobs',
        searchTerm: '',
        pageSize: 10,
        pageNumber: 1,
        totalNumber: 0,
        subcriptionFieldValues: clone(fields),
    },
    [
        {
            label: 'loadAdminCustomerList',
            formatter: (data, request) => {
                request.body = {
                    query: `{\n  customerlist(searchTerm: "${data.searchTerm}", page: ${data.pageNumber}, pageSize: ${data.pageSize}){\n    count,\n    customerlist{\n    id,\n    name,\n    email,\n    createdAt,\n    isActive,\n   subscriptions {\n    end,\n    name,\n    id,\n    type,\n    start\n  }\n   }\n}\n}`,
                };
                return request;
            },
            request: {
                url: `${CONFIG.serverUrl}/graphql/customer`,
                method: 'post',
            },
            outputField: 'adminCustomerListResponse',
            statusLabel: 'adminCustomerListStatus',
            error: () => ({ message: 'Something went wrong while loading customer list' }),
            parse: (output) => {
                const customerList = get(output, ['data', 'data', 'customerlist', 'customerlist'], []);
                const totalNumber = get(output, ['data', 'data', 'customerlist', 'count'], 0);
                AdminCustomerListStore.set({
                    customerList,
                    totalNumber,
                });
                return output;
            },
        },
        {
            label: 'loadUpdateCustomerStatus',
            outputField: 'updateCustomerResponse',
            statusLabel: 'updateCustomerStatus',
            request: {
                url: `${CONFIG.serverUrl}/graphql/customer`,
                method: 'POST',
            },
            formatter: (data, request) => {
                const {
                    modalValues,
                } = data;
                const {
                    isActive = false,
                    activeItemId = '',
                } = modalValues;
                request.body = {
                    query: 'mutation update_customer($id: ID!, $isActive: Boolean!){\n  customerUpdate(id: $id, isActive:$isActive){\n    customer{\n      id,\n    }\n  }\n}',
                    variables: {
                        id: activeItemId,
                        isActive,
                    },
                    operationName: 'update_customer',
                };
                return request;
            },
            error: () => ({ message: 'Something went wrong while updating customer status' }),
            parse: (output) => {
                AdminCustomerListStore.resetData();
                AdminCustomerListStore.load('loadAdminCustomerList');
                if (output.errors || output.data.errors) {
                    const outputRender = output;
                    outputRender.status = 'error';
                    const errorMessage = get(output.data, ['errors', '0', 'message'], '');
                    if (errorMessage) {
                        outputRender.message = errorMessage;
                    } else {
                        outputRender.message = 'Something went wrong while updating customer status';
                    }
                    return outputRender;
                }
                return { message: 'Customer Status Updated Successfully' };
            },
        },
        {
            label: 'loadRemoveClient',
            outputField: 'removeClientResponse',
            statusLabel: 'removeClientStatus',
            request: {
                url: `${CONFIG.serverUrl}/graphql/customer`,
                method: 'POST',
            },
            formatter: (data, request) => {
                const {
                    modalValues,
                } = data;
                const {
                    activeItemId = '',
                } = modalValues;
                request.body = {
                    query: 'mutation delete_customer($id: ID!) { customerDelete(id:$id) { customer{ id } } }',
                    variables: {
                        id: activeItemId,
                    },
                    operationName: 'delete_customer',
                };
                return request;
            },
            error: () => ({ message: 'Something went wrong while removing client' }),
            parse: (output) => {
                AdminCustomerListStore.resetData();
                AdminCustomerListStore.load('loadAdminCustomerList');
                if (output.errors || output.data.errors) {
                    const outputRender = output;
                    outputRender.status = 'error';
                    const errorMessage = get(output.data, ['errors', '0', 'message'], '');
                    if (errorMessage) {
                        outputRender.message = errorMessage;
                    } else {
                        outputRender.message = 'Something went wrong while removing client';
                    }
                    return outputRender;
                }
                return { message: 'Client Removed Successfully' };
            },
        },
    ],
);
export default AdminCustomerListStore;
