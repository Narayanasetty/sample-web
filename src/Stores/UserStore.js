import Store from 'Lib/store';
import { eventStream } from 'Streams';
import { getConfig } from 'Utils';
import { get } from 'lodash';

const CONFIG = getConfig();

const AdminUserStore = new Store(
    {
        userDetails: {},
    },
    [
        {
            label: 'LoadUser',
            formatter: (data, request) => request,
            request: {
                url: `${CONFIG.serverUrl}/api/v1/get_user`,
                method: 'get',
            },
            outputField: 'userDetails',
            statusLabel: 'userLoadedStatus',
            error: (error) => {
                eventStream.next('login');
                return error;
            },
            parse: (output) => {
                eventStream.next('userLoaded');
                return get(output, ['data'], {});
            },
        },
    ],
);
export default AdminUserStore;
