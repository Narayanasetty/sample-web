import Store from 'Lib/store';
import { REGEXCONSTANTS } from 'Constants/regex';
import { getConfig } from 'Utils';
import AdminUserStore from './UserStore';


const CONFIG = getConfig();

const LoginStore = new Store(
    {
        email: {
            value: 'admin@thanos.com',
            regex: REGEXCONSTANTS.email,
            isValid: false,
            isInValid: false,
            validationMsg: {
                touched: '',
                isRequired: '',
                isValidState: '',
            },
            touched: false,
            isRequired: true,
        },
        password: {
            value: 'admin@123',
            regex: REGEXCONSTANTS.password,
            isValidState: true,
            isValid: false,
            isInValid: false,
            validationMsg: '',
            touched: false,
            isRequired: true,
        },
        showSubDomain: true,
        showForgotPassword: true,
        showCreateAccount: true,
    },
    [
        {
            label: 'login',
            formatter: (data, request) => {
                request.body = {
                    email: data.email.value,
                    password: data.password.value,
                };
                return request;
            },
            outputField: 'loginResponse',
            statusLabel: 'loginStatus',
            request: {
                url: `${CONFIG.serverUrl}/api/v1/login`,
                method: 'POST',
            },
            parse: (output) => {
                AdminUserStore.load('LoadUser');
                return output;
            },
        },

    ],
);
export default LoginStore;
